#!/bin/bash

echo 'A variável $0 exibe o nome do script:' $0
sleep 2
echo 'A variável $$ exibe o PID do processo em execução:' $$
sleep 2
echo 'A variável $! exibe o PID do último comando executado:' $!
sleep 2
echo 'A variável $# exibe a quantidade de argumentos digitados na linha de comando:' $#
sleep 2
echo 'A variável $* exibe todos os argumentos passados na linha de comando:' $*
sleep 2 
echo 'A variável $n retorna o número n de argumentos do programa:' $n
sleep 2
echo 'A variável $_ mostra o nome absoluto do arquivo do shell ou script que está sendo executado como passado na lista de argumentos:' $_
sleep 2
echo 'A variável $USER retorna o nome do usuário:' $USER
sleep 2
echo 'A variável $UID retorna o ID do usuário:' $UID
sleep 2
echo 'A variável $PWD retorna o diretório atual:' $PWD
sleep 2
echo 'A variável $HOSTNAME mostra o nome do computador:' $HOSTNAME
sleep 2
echo 'A variável $PPID obtém o ID do processso pai:' $PPID
sleep 2
echo 'A variável $TMOUT mostra o timeout padrão do comando read do shell:' $TMOUT
sleep 2
echo 'A variável $RANDOM obtém um número aleátorio:' $RANDOM 
sleep 2
echo 'A variável $PIPESTATUS retorna o status de uma saída pipe:' $PIPESTATUS
