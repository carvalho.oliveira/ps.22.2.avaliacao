#!/bin/bash

echo 			"..............................."
echo 			"|RESUMO SOBRE REDIRECIONADORES|"
echo 			"..............................."
sleep 3

echo "> >>  ou 1> 1>> SAÍDA"
echo "Redireciona a saída padrão do comando para um arquivo."
echo "'>' sobrescreve o arquivo em questão."
echo "'>>' adiciona a saída do comando ao final do arquivo, sem apagá-lo."
echo "................................................................................"
sleep 3
echo "2> 2>> SAÍDA"
echo "Redireciona a saída padrão de erros do comando para um arquivo."
echo 		"'2>' sobrescreve o arquivo em questão."
echo 		"'2>>' adiciona a saída do comando ao final do arquivo, sem apagá-lo."
echo "................................................................................"
sleep 3
echo "&> &>> SAÍDA"
echo "Redireciona todas as saídas do comando para um arquivo."	
echo 		"'&>' sobrescreve o arquivo em questão."
echo 		"'&>>' adiciona a saída do comando ao final do arquivo, sem apagá-lo."
echo "................................................................................"
sleep 3
echo "< ENTRADA"
echo 	"Redireciona o arquivo para a entrada padrão do comando."
echo 		"Ex.: ./teste.sh < arq.txt"
echo "................................................................................"
echo "<<  ENTRADA"
echo 	"Permite redirecionar a entrada padrão do comando para o documento escrito no bash."
echo "................................................................................"
sleep 3
echo "<<<  ENTRADA"
echo "Permite redirecionar a entrada padrão do comando para a string escrita no bash."
echo 		"Equivalente a executar: echo "teste" | grep t"
echo 		"Seria executar: grep t <<< 'teste'"
sleep 3
echo -e "\033[0;35m........................\033[0m"
echo -e "\033[0;35mEXEMPLOS DE USO:\033[0m"
echo -e "\033[0;35m........................\033[0m"
echo -e	"\033[0;35mExecutaremos o comando: 'echo 'Oi, tudo bem?' &>> teste.txt' que irá redirecionar a saída padrão e com erros para o arq 'teste.txt'\033[0m"
echo "Oi, tudo bem?" &>> teste.txt
echo -e 	"\033[0;35mAgora daremos um 'cat teste.txt' para vermos que executou o que foi dito acima.\033[0m"
cat teste.txt  
